
DESCRIPTION
-----------
Drupal Middleware is used to establish XML-RPC based data exchange 
between drupal and external systems inculding another drupal 
instances. Data can be replicated in many directions using 
different routes with different rules applied. Creation, updates 
and removals are separated. Pluggable connectors are take care of 
system-specific implementations.

INSTALLATION
------------
Download and enable module.

BASIC SETUP
-----------
1. Enable module;
2. Go to Admin -> Content -> Middleware Client;
3. Enter instance url with path to Drupal's xmlrpc.php e.g. http://myinstance/xmlrpc.php;
4. Enter server secret Token. You can generate one using Middleware server;
5. Enter URL and Token for the Server.

To enable user replication from this instance go to Admin -> User -> Settings, then
in the Middleware publication settings select "Publicate users on middleware" option.

See README.txt for Middleware Server module for information on Server setup.


REQUIREMENTS
------------
Mass user publication is available under Admin -> User -> Middleware path. 
Job_queue module is a requirement to perform such an opperation.  

FAQ
---

LIMITATIONS
-----------
Drupal Middleware is under heavy development. It's still possible for
architecture design to be cardinally changed so back compatibility for
future releases is not guaranteed.

CREDITS
-------
Design and implementation by Mikhail Khorpyakov