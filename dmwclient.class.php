<?php

/**
 * DMW client to work with middleware
 */
class DMWClient 
{
	private $token;
	private $instance_url;
	private $mw_url;
	
	function __construct($instance_url, $instance_token, $mw_url) {
		$this->token        = $instance_token;
		$this->instance_url = $instance_url;
		$this->mw_url       = $mw_url;            
	}
	        		
	public function publish($type, $action, $data)
	{
		$request = xmlrpc_encode_request('mw.publish', array($this->instance_url, $this->token, $type, $action, $data), array('escaping' => 'markup', 'encoding' => 'UTF-8'));
		$context = stream_context_create(array('http' => array(
		    'method'  => 'POST',
		    'header'  => 'Content-Type: text/xml',
		    'content' => $request    
		)));
		$file = file_get_contents($this->mw_url . '/xmlrpc.php', false, $context);
		$response = xmlrpc_decode($file);
		if ($response) {
			if ((isset($response['faultCode'])) and ($response['faultCode'] != '')) {
				return $response['faultCode'];
			} else {
				return $response;
			}
		} else {
			return FALSE;
		}
	}		
}	
?>