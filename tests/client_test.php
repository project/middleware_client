<?php
    define('SIMPLE_TEST_FOLDER', 'D:\soft\php\simpletest');
    
    // Middleware settings
    define('MW_INSTANCE_URL',   'http://devserver:84/kitairu');
    define('MW_URL',            'http://devserver:84/middleware');
    define('MW_INSTANCE_TOKEN', '1f756723099f0d13ffc34f1e6aded912');
    
    
    require_once SIMPLE_TEST_FOLDER . '\autorun.php';
    require_once '../dmwclient.class.php';
    
    class ClientTestCase extends UnitTestCase {
    	
    	/*
    	function testPublish() {
    		$data = array('login' => 'test', 'password' => 'test', 'email' => 'test@test.ru');
    		$client = new DMWClient(MW_INSTANCE_URL, MW_INSTANCE_TOKEN, MW_URL);    		    		
    		$this->assertTrue($client->publish('user', 'create', $data));    		
    	}
    	*/
    	
    	/*
    	function testPublishProfile() {
	        // Construct test account
	        $account = array(
	           'id'       => 1,
	           'login'    => 'khorpyakov',
	           'date'     => date('Y-m-d'),
	           'state_id' => 1,
	           'name1'    => 'Русский',
	           'name2'    => 'English name',
	           'name3'    => '我的淘宝',
	           'keywords1' => 'Русский',
	           'keywords2' => 'English keywords',
	           'keywords3' => '我的淘宝',
	           'info1'   => 'Русский текст',
	           'info2'   => 'English description',
	           'info3'   => '我的淘宝',
	           'range'   => array(702),
	           'person'  => 'Vasya pupkin',
	           'town'    => 'Voronezh',
	           'address' => 'Lenin dadao',
	           'phone'   => '355000',
	           'msn'     => 'vasya@msn.com',
	           'skype'   => 'vasya',
	           'icq'     => '37755921',
	           'www'     => 'http://www.vaysa.com',
	           'pin'      => '123',
	           'def_l_id' => 1,
	           'status'   => 2,
	//         'x'
	//         'y'
	//         'pic'
	           'vend'          => 1,
	           'cust'          => 0,
	           'items_num'     => 10,
	           'set_items_num' => 10,
	           'score'         => 0,
	           'subscribe'     => 0,
	           'notified'      => 1,
	           'show_contact'  => 0,
	        );

            $client = new DMWClient(MW_INSTANCE_URL, MW_INSTANCE_TOKEN, MW_URL);
            $result = $client->publish('content_profile', 'create', $account);                        
            $this->assertEqual(0, $result, "Error code: >>$result<<");	        
    	}*/
    	
    	function testPublishUpload() {
    		// Generate test file data
    		$image = imagecreate(100, 100);
    		ob_start();
    		imagejpeg($image);
    		$fcontents = ob_get_contents();
    		ob_end_clean();

    		$upload = array('file' => base64_encode($fcontents), 'id' => 1, 'filename' => 'test');

            $client = new DMWClient(MW_INSTANCE_URL, MW_INSTANCE_TOKEN, MW_URL);
            $result = $client->publish('upload', 'create', $upload);                        
            $this->assertEqual(0, $result, "Error code: >>$result<<");              		
    	}
    }
?>